;;; -*- encoding: utf-8 -*-
;;;
;;; juego.lisp
;;; Principal

(load "tablero.lisp")
(load "movimientos/movimientos.lisp")
(load "ia.lisp")
(load "pedir.lisp")

(defvar *llamadas* 0)

(defun ajedrez ()

  ;; inicializa
  (let* 
    (
      (jugador1 (make-jugador 
        :tipo 'manual 
        :color 'blanco))
      (jugador2 (make-jugador 
        :tipo 'auto 
        :color 'negro))
      ;;(la-mesa (crear-tablero-inicial-pruebas))
	    (la-mesa (crear-tablero-inicial-blancas-abajo))
	    (jugador-actual)
	    (movimiento-actual)
   )

    (setf jugador-actual jugador1)

    ;; bucle
    (loop
       
       (muestra-tablero la-mesa)
       (setf *llamadas* 0)
       ;; Jaque mate?
       (if (jaque-mate la-mesa jugador-actual)
	   ;; Si -> fuera
	   (progn
	     (format t "Pierde el color ~A.~%"
		     (jugador-color jugador-actual))
	     (return))
	   ;; no -> carga movimiento
	   (setf movimiento-actual (carga-movimiento la-mesa
						     jugador-actual)))
       
       ;; comprueba movimiento - incluido los enroques
       (if (valido la-mesa
		   movimiento-actual
		   (jugador-color jugador-actual))
	   ;; valido -> ejecuta movimiento
	   (progn
	     (ejecuta-movimiento la-mesa movimiento-actual)
	     (if (eq jugador-actual jugador1)
		 (setf jugador-actual jugador2)
		 (setf jugador-actual jugador1)))
	   ;; no valido -> automatico?
	   (if (eq (jugador-tipo jugador-actual) 'auto)
	       ;; si -> error
	       (progn
		 (print "Movimiento")
		 (return))
	       ;; no -> vuelve a preguntar
	       (print "Movimiento no valido"))))))


(defun carga-movimiento (la-mesa el-jugador)
  (if (eq (jugador-tipo el-jugador) 'manual)
      (lee-movimiento)
      (decide-movimiento la-mesa (jugador-color el-jugador))))




(defun prueba-posiciones-libres-peon ()
  (let ((tablero (crear-tablero-inicial-blancas-abajo)))
    (muestra-tablero tablero)
    (print (posiciones-libres-peon tablero '#s(posicion fila 6 columna 0)))))

(defun prueba-posibles-libres-peon ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (print (posibles-libres-peon tablero '#s(posicion fila 6 columna 0)))))

(defun prueba-posiciones-ataque-peon ()
  (let ((tablero (crear-tablero-inicial-blancas-abajo)))
    (muestra-tablero tablero)
    (print (posiciones-ataque-peon tablero '#s(posicion fila 6 columna 2)))))

(defun prueba-posibles-ataque-peon ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (print (posibles-ataque-peon tablero '#s(posicion fila 4 columna 1)))))

(defun prueba-posibles-peon ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (dotimes (i 20000)
      (posibles-peon tablero '#s(posicion fila 5 columna 2)))))

(defun prueba-ejecuta-movimiento ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (ejecuta-movimiento tablero '#s(movimiento
				    desde #s(posicion fila 4
						      columna 1)
				    hasta #s(posicion fila 5
						      columna 0)))
    (print tablero)
    (muestra-tablero tablero)))

(defun prueba-sucesores ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (dotimes (i 642)
    (sucesores tablero 'blanco))))

(defun prueba-posiciones-caballo ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (print (posiciones-caballo tablero '#s(posicion fila 7 columna 1)))))

(defun prueba-posibles-caballo ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (print (posibles-caballo tablero '#s(posicion fila 7 columna 1)))))

(defun stress ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (dotimes (a 10000)
      (sucesores tablero 'blanco))))

(defun prueba-posibles-torre ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (print (posibles-torre tablero '#s(posicion fila 3 columna 1)))))

(defun prueba-posibles-alfil ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (print (posibles-alfil tablero '#s(posicion fila 7 columna 2)))))

(defun prueba-posibles-dama ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (print (posibles-dama tablero '#s(posicion fila 7 columna 4)))))

(defun prueba-posibles-rey ()
  (let ((tablero (crear-tablero-inicial-pruebas)))
    (muestra-tablero tablero)
    (print (posibles-rey tablero '#s(posicion fila 7 columna 4)))))

(ajedrez)

;(ajz)
;(prueba-posibles-alfil)
;(prueba-ejecuta-movimiento)
;(prueba-sucesores)
;(stress)
;(prueba-posibles-dama)
;(prueba-posibles-rey)
