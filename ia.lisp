;;; -*- encoding: utf-8 -*-
;;;
;;; ia.lisp
;;; Inteligencia Artificial
;;; http://chessprogramming.wikispaces.com/Alpha-Beta


;; Las piezas pesan mas o menos en funcion de lo cerca que esten del
;; centro del tablero.
(defconstant +peso-posicion-alfil-caballo+
  (make-array 8 :initial-contents '(10 10 30 60 60 30 10 10)))

(defconstant +peso-posicion-torre+
  (make-array 8 :initial-contents '(60 30 30 10 10 30 30 60)))

(defconstant +peso-posicion-peon-v+
  (make-array 8 :initial-contents '(0 0 5 10 30 100 250 900)))

(defconstant +peso-posicion-peon-h+
  (make-array 8 :initial-contents '(2 5 10 20 20 10 5 2)))




(defun decide-movimiento (tablero color)
  (second (multiple-value-list
	   (alfabeta tablero 3 -1000000 1000000 color))))


;función alfabeta(nodo, profundidad, α, β, jugadorMaximizador)
;    si nodo es un nodo terminal o profundidad = 0
;        devolver el valor heurístico del nodo
;    si jugadorMaximizador
;        para cada hijo de nodo
;            si β≤α
;                break (* poda β *)
;        devolver α
;    si no
;        para cada hijo de nodo
;            β := min(β, alfabeta(hijo, profundidad-1, α, β, TRUE))
;                break (* poda α *)
;

(defun alfabeta (tablero profundidad alfa beta color)
  (if (eq profundidad 0)
      (fev tablero)
      (let ((movimiento)
	    (sucesores (sucesores tablero color)))
	(dolist (un-movimiento sucesores)
	  (when (valido-rey tablero un-movimiento color)
	    (when (null movimiento) (setf movimiento un-movimiento))
	    (let* ((nuevo-tablero (ejecuta-movimiento (copia-tablero tablero)
						      un-movimiento))
		   (val (alfabeta nuevo-tablero (- profundidad 1)
				  alfa beta (cambia-color color))))
	      (if (eq color 'blanco)
		  (progn
		    (when (> val alfa)
		      (setf alfa val)
		      (setf movimiento un-movimiento))
		    (when (> alfa beta)
		      (values alfa movimiento)))
		  (progn
		    (when (< val beta)
		      (setf beta val)
		      (setf movimiento un-movimiento))
		    (when (> alfa beta)
		      (values beta movimiento)))))))
	(if (eq color 'blanco)
	    (values alfa movimiento)
	    (values beta movimiento)))))

(defun fev (tablero)
  (let ((fev 0))
    (dotimes (i 8)
      (dotimes (j 8)
	(let ((pieza (aref (tablero-tablero tablero) i j)))
	  (cond ((eq pieza 'PB)
		 (setf fev (+ fev
			      100
			      (if (eq
				   (tablero-posicion-inicial-blancas tablero)
				   'abajo)
				  (aref +peso-posicion-peon-v+ (- 7 i))
				  (aref +peso-posicion-peon-v+ i))
			      (aref +peso-posicion-peon-h+ j))))
		((eq pieza 'PN)
		 (setf fev (- fev
			      100
			      (if (eq
				   (tablero-posicion-inicial-blancas tablero)
				   'abajo)
				  (aref +peso-posicion-peon-v+ i)
				  (aref +peso-posicion-peon-v+ (- 7 i)))
			      (aref +peso-posicion-peon-h+ j))))
		((eq pieza 'TB)
		 (setf fev (+ fev
			      500
			      (aref +peso-posicion-torre+ i)
			      (aref +peso-posicion-torre+ j))))
		((eq pieza 'TN)
		 (setf fev (- fev
			      500
			      (aref +peso-posicion-torre+ i)
			      (aref +peso-posicion-torre+ j))))
		((eq pieza 'AB)
		 (setf fev (+ fev
			      300
			      (aref +peso-posicion-alfil-caballo+ i)
			      (aref +peso-posicion-alfil-caballo+ j))))
		((eq pieza 'AN)
		 (setf fev (- fev
			      300
			      (aref +peso-posicion-alfil-caballo+ i)
			      (aref +peso-posicion-alfil-caballo+ j))))
		((eq pieza 'CB)
		 (setf fev (+ fev
			      300
			      (aref +peso-posicion-alfil-caballo+ i)
			      (aref +peso-posicion-alfil-caballo+ j))))
		((eq pieza 'CN)
		 (setf fev (- fev
			      300
			      (aref +peso-posicion-alfil-caballo+ i)
			      (aref +peso-posicion-alfil-caballo+ j))))
		((eq pieza 'RB)
		 (setf fev (+ fev 1000)))
		((eq pieza 'RN)
		 (setf fev (- fev 1000)))
		((eq pieza 'DB)
		 (setf fev (+ fev 900)))
		((eq pieza 'DN)
		 (setf fev (- fev 900)))))))
    fev))
		

