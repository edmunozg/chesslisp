;;; -*- encoding: utf-8 -*-
;;;
;;; torres.lisp
;;; Movimiento de las torres (Ataque y movimiento en horizontal)


(defun posibles-torre-blanca (tablero posicion)
  (let ((posiciones '()))
    ;; Las doce
    (dotimes (fila (posicion-fila posicion))
      (cond ((eq (aref (tablero-tablero tablero)
		       (- (posicion-fila posicion) (1+ fila))
		       (posicion-columna posicion))
		 'VV)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (- (posicion-fila posicion) (1+ fila))
			  :columna (posicion-columna posicion))
			 posiciones)))
	    ((eq (aref (tablero-negras tablero)
		       (- (posicion-fila posicion) (1+ fila))
		       (posicion-columna posicion))
		 '1)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (- (posicion-fila posicion) (1+ fila))
			  :columna (posicion-columna posicion))
			 posiciones))
	     (return nil))
	    (t (return nil))))
    ;; Las tres
    (dotimes (columna (- 7 (posicion-columna posicion)))
      (cond ((eq (aref (tablero-tablero tablero)
		       (posicion-fila posicion)
		       (+ (posicion-columna posicion) columna 1))
		 'VV)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (posicion-fila posicion)
			  :columna (+ (posicion-columna posicion) columna 1))
			 posiciones)))
	    ((eq (aref (tablero-negras tablero)
		       (posicion-fila posicion)
		       (+ (posicion-columna posicion) columna 1))
		 '1)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (posicion-fila posicion)
			  :columna (+ (posicion-columna posicion) columna 1))
			 posiciones))
	     (return nil))
	    (t (return nil))))
    ;; Las seis
    (dotimes (fila (- 7 (posicion-fila posicion)))
      (cond ((eq (aref (tablero-tablero tablero)
		       (+ (posicion-fila posicion) fila 1)
		       (posicion-columna posicion))
		 'VV)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (+ (posicion-fila posicion) fila 1)
			  :columna (posicion-columna posicion))
			 posiciones)))
	    ((eq (aref (tablero-negras tablero)
		       (+ (posicion-fila posicion) fila 1)
		       (posicion-columna posicion))
		 '1)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (+ (posicion-fila posicion) fila 1)
			  :columna (posicion-columna posicion))
			 posiciones))
	     (return nil))
	    (t (return nil))))
    ;; Las nueve
    (dotimes (columna (posicion-columna posicion))
      (cond ((eq (aref (tablero-tablero tablero)
		       (posicion-fila posicion)
		       (- (posicion-columna posicion) (1+ columna)))
		 'VV)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (posicion-fila posicion)
			  :columna (- (posicion-columna posicion)
				      (1+ columna)))
			 posiciones)))
	    ((eq (aref (tablero-negras tablero)
		       (posicion-fila posicion)
		       (- (posicion-columna posicion) (1+ columna)))
		 '1)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (posicion-fila posicion)
			  :columna (- (posicion-columna posicion)
				      (1+ columna)))
			 posiciones))
	     (return nil))
	    (t (return nil))))
    posiciones))


(defun posibles-torre-negra (tablero posicion)
  (let ((posiciones '()))
    ;; Las doce
    (dotimes (fila (posicion-fila posicion))
      (cond ((eq (aref (tablero-tablero tablero)
		       (- (posicion-fila posicion) (1+ fila))
		       (posicion-columna posicion))
		 'VV)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (- (posicion-fila posicion) (1+ fila))
			  :columna (posicion-columna posicion))
			 posiciones)))
	    ((eq (aref (tablero-blancas tablero)
		       (- (posicion-fila posicion) (1+ fila))
		       (posicion-columna posicion))
		 '1)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (- (posicion-fila posicion) (1+ fila))
			  :columna (posicion-columna posicion))
			 posiciones))
	     (return nil))
	    (t (return nil))))
    ;; Las tres
    (dotimes (columna (- 7 (posicion-columna posicion)))
      (cond ((eq (aref (tablero-tablero tablero)
		       (posicion-fila posicion)
		       (+ (posicion-columna posicion) columna 1))
		 'VV)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (posicion-fila posicion)
			  :columna (+ (posicion-columna posicion) columna 1))
			 posiciones)))
	    ((eq (aref (tablero-blancas tablero)
		       (posicion-fila posicion)
		       (+ (posicion-columna posicion) columna 1))
		 '1)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (posicion-fila posicion)
			  :columna (+ (posicion-columna posicion) columna 1))
			 posiciones))
	     (return nil))
	    (t (return nil))))
    ;; Las seis
    (dotimes (fila (- 7 (posicion-fila posicion)))
      (cond ((eq (aref (tablero-tablero tablero)
		       (+ (posicion-fila posicion) fila 1)
		       (posicion-columna posicion))
		 'VV)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (+ (posicion-fila posicion) fila 1)
			  :columna (posicion-columna posicion))
			 posiciones)))
	    ((eq (aref (tablero-blancas tablero)
		       (+ (posicion-fila posicion) fila 1)
		       (posicion-columna posicion))
		 '1)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (+ (posicion-fila posicion) fila 1)
			  :columna (posicion-columna posicion))
			 posiciones))
	     (return nil))
	    (t (return nil))))
    ;; Las nueve
    (dotimes (columna (posicion-columna posicion))
      (cond ((eq (aref (tablero-tablero tablero)
		       (posicion-fila posicion)
		       (- (posicion-columna posicion) (1+ columna)))
		 'VV)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (posicion-fila posicion)
			  :columna (- (posicion-columna posicion)
				      (1+ columna)))
			 posiciones)))
	    ((eq (aref (tablero-blancas tablero)
		       (posicion-fila posicion)
		       (- (posicion-columna posicion) (1+ columna)))
		 '1)
	     (setf posiciones
		   (cons (make-posicion
			  :fila (posicion-fila posicion)
			  :columna (- (posicion-columna posicion)
				      (1+ columna)))
			 posiciones))
	     (return nil))
	    (t (return nil))))
    posiciones))





(defun posibles-torre (tablero posicion)
  (if (eq (aref (tablero-tablero tablero)
		(posicion-fila posicion)
		(posicion-columna posicion))
	  'TB)
      ;; La torre es blanca
      (posibles-torre-blanca tablero posicion)
    ;; La torre es negra
    (posibles-torre-negra tablero posicion)))
