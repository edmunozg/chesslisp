;;; -*- encoding: utf-8 -*-
;;;
;;; alfiles.lisp
;;; Movimiento de los alfiles (Ataque y movimiento en diagonal)

(defun posible-mueve-blanca (tablero posicion)
  (eq (aref (tablero-tablero tablero)
	    (posicion-fila posicion)
	    (posicion-columna posicion)) 'VV))

(defun posible-come-blanca (tablero posicion)
  (eq (aref (tablero-negras tablero)
	    (posicion-fila posicion)
	    (posicion-columna posicion)) '1))

(defun posible-mueve-negra (tablero posicion)
  (eq (aref (tablero-tablero tablero)
	    (posicion-fila posicion)
	    (posicion-columna posicion)) 'VV))

(defun posible-come-negra (tablero posicion)
  (eq (aref (tablero-blancas tablero)
	    (posicion-fila posicion)
	    (posicion-columna posicion)) '1))

(defun desplaza-posicion-diagonal (posicion f c)
  (make-posicion :fila (+ (posicion-fila posicion) f)
		 :columna (+ (posicion-columna posicion) c)))







(defun posibles-alfil-blanco (tablero posicion)
  (let ((posiciones '()))
    ;; NE
    (dotimes (c (min (posicion-fila posicion)
		     (- 7 (posicion-columna posicion))))
      (let ((nueva-posicion
	     (desplaza-posicion-diagonal posicion (- (1+ c)) (1+ c))))
	(cond ((posible-mueve-blanca tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones)))
	      ((posible-come-blanca tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones))
	       (return nil))
	      (t (return nil)))))

    ;; SE
    (dotimes (c (min (- 7 (posicion-fila posicion))
		     (- 7 (posicion-columna posicion))))
      (let ((nueva-posicion
	     (desplaza-posicion-diagonal posicion (1+ c) (1+ c))))
	(cond ((posible-mueve-blanca tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones)))
	      ((posible-come-blanca tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones))
	       (return nil))
	      (t (return nil)))))

    ;; SO
    (dotimes (c (min (- 7 (posicion-fila posicion))
		     (posicion-columna posicion)))
      (let ((nueva-posicion
	     (desplaza-posicion-diagonal posicion (1+ c) (- (1+ c)))))
	(cond ((posible-mueve-blanca tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones)))
	      ((posible-come-blanca tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones))
	       (return nil))
	      (t (return nil)))))

    ;; NO
    (dotimes (c (min (posicion-fila posicion)
		     (posicion-columna posicion)))
      (let ((nueva-posicion
	     (desplaza-posicion-diagonal posicion (- (1+ c)) (- (1+ c)))))
	(cond ((posible-mueve-blanca tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones)))
	      ((posible-come-blanca tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones))
	       (return nil))
	      (t (return nil)))))
    posiciones))





(defun posibles-alfil-negro (tablero posicion)
  (let ((posiciones '()))
    ;; NE
    (dotimes (c (min (posicion-fila posicion)
		     (- 7 (posicion-columna posicion))))
      (let ((nueva-posicion
	     (desplaza-posicion-diagonal posicion (- (1+ c)) (1+ c))))
	(cond ((posible-mueve-negra tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones)))
	      ((posible-come-negra tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones))
	       (return nil))
	      (t (return nil)))))

    ;; SE
    (dotimes (c (min (- 7 (posicion-fila posicion))
		     (- 7 (posicion-columna posicion))))
      (let ((nueva-posicion
	     (desplaza-posicion-diagonal posicion (1+ c) (1+ c))))
	(cond ((posible-mueve-negra tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones)))
	      ((posible-come-negra tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones))
	       (return nil))
	      (t (return nil)))))

    ;; SO
    (dotimes (c (min (- 7 (posicion-fila posicion))
		     (posicion-columna posicion)))
      (let ((nueva-posicion
	     (desplaza-posicion-diagonal posicion (1+ c) (- (1+ c)))))
	(cond ((posible-mueve-negra tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones)))
	      ((posible-come-negra tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones))
	       (return nil))
	      (t (return nil)))))

    ;; NO
    (dotimes (c (min (posicion-fila posicion)
		     (posicion-columna posicion)))
      (let ((nueva-posicion
	     (desplaza-posicion-diagonal posicion (- (1+ c)) (- (1+ c)))))
	(cond ((posible-mueve-negra tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones)))
	      ((posible-come-negra tablero nueva-posicion)
	       (setf posiciones (cons nueva-posicion posiciones))
	       (return nil))
	      (t (return nil)))))
    posiciones))





(defun posibles-alfil (tablero posicion)
  (if (eq (aref (tablero-tablero tablero)
		(posicion-fila posicion)
		(posicion-columna posicion))
	  'AB)
      ;; El alfil es blanco
      (posibles-alfil-blanco tablero posicion)
    ;; El alfil es negro
    (posibles-alfil-negro tablero posicion)))
