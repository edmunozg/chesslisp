;;; -*- encoding: utf-8 -*-
;;;
;;; damas.lisp
;;; Movimientos de las damas(Ataque y movimienot en todas partes) Es la más poderoza del juego


(defun posibles-dama (tablero posicion)
  (let* ((funciones-torre '((blanca posibles-torre-blanca)
			    (negra posibles-torre-negra)))
	 (funciones-alfil '((blanca posibles-alfil-blanco)
			    (negra posibles-alfil-negro)))
	 (color (if (eq (aref (tablero-tablero tablero)
			      (posicion-fila posicion)
			      (posicion-columna posicion))
			'DB)
		    'blanca
		  'negra))
	 (funcion-torre (second (assoc color funciones-torre)))
	 (funcion-alfil (second (assoc color funciones-alfil)))
	 (posiciones '()))

    (setf posiciones (funcall funcion-torre tablero posicion))
    (setf posiciones (append posiciones
			     (funcall funcion-alfil tablero posicion)))))
