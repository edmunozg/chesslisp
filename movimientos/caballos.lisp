;;; -*- encoding: utf-8 -*-
;;;
;;; caballos.lisp
;;; Movimiento de los caballos(Ataque y movimienot en L)



;;; Lugares amenazados por un caballo.
(defun posiciones-caballo (tablero posicion)
  (let ((posiciones '()))
    ;; La una
    (if (and (> (posicion-fila posicion) 1)
	     (< (posicion-columna posicion) 7))
	(setf posiciones (cons (make-posicion
				:fila (- (posicion-fila posicion) 2)
				:columna (1+ (posicion-columna posicion)))
			       posiciones)))
    ;; Las dos
    (if (and (> (posicion-fila posicion) 0)
	     (< (posicion-columna posicion) 6))
	(setf posiciones (cons (make-posicion
				:fila (1- (posicion-fila posicion))
				:columna (+ (posicion-columna posicion) 2))
			       posiciones)))
    ;; Las cuatro
    (if (and (< (posicion-fila posicion) 7)
	     (< (posicion-columna posicion) 6))
	(setf posiciones (cons (make-posicion
				:fila (1+ (posicion-fila posicion))
				:columna (+ (posicion-columna posicion) 2))
			       posiciones)))
    ;; Las cinco
    (if (and (< (posicion-fila posicion) 6)
	     (< (posicion-columna posicion) 7))
	(setf posiciones (cons (make-posicion
				:fila (+ (posicion-fila posicion) 2)
				:columna (1+ (posicion-columna posicion)))
			       posiciones)))
    ;; Las siete
    (if (and (< (posicion-fila posicion) 6)
	     (> (posicion-columna posicion) 0))
	(setf posiciones (cons (make-posicion
				:fila (+ (posicion-fila posicion) 2)
				:columna (1- (posicion-columna posicion)))
			       posiciones)))
    ;; Las ocho
    (if (and (< (posicion-fila posicion) 7)
	     (> (posicion-columna posicion) 1))
	(setf posiciones (cons (make-posicion
				:fila (1+ (posicion-fila posicion))
				:columna (- (posicion-columna posicion) 2))
			       posiciones)))
    ;; Las diez
    (if (and (> (posicion-fila posicion) 0)
	     (> (posicion-columna posicion) 1))
	(setf posiciones (cons (make-posicion
				:fila (1- (posicion-fila posicion))
				:columna (- (posicion-columna posicion) 2))
			       posiciones)))
    ;; Las once
    (if (and (> (posicion-fila posicion) 1)
	     (> (posicion-columna posicion) 0))
	(setf posiciones (cons (make-posicion
				:fila (- (posicion-fila posicion) 2)
				:columna (1- (posicion-columna posicion)))
			       posiciones)))
posiciones))





(defun posibles-caballo (tablero posicion)
  (let ((posiciones '()))
    (dolist (una-posicion (posiciones-caballo tablero posicion))
      (if (eq (aref (tablero-tablero tablero)
		    (posicion-fila posicion)
		    (posicion-columna posicion))
	      'CB)
	  ;; El caballo es blanco
	  (if (eq (aref (tablero-blancas tablero)
			(posicion-fila una-posicion)
			(posicion-columna una-posicion))
		  '0)
	      (setf posiciones (cons una-posicion posiciones)))
	;; El caballo es negro
	(if (eq (aref (tablero-negras tablero)
		      (posicion-fila una-posicion)
		      (posicion-columna una-posicion))
		'0)
	    (setf posiciones (cons una-posicion posiciones)))))
    posiciones))