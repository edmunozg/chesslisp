;;; -*- encoding: utf-8 -*-
;;;
;;; movimientos.lisp
;;; Funciones relativas al movimiento de piezas.



(load "movimientos/peones.lisp")
(load "movimientos/caballos.lisp")
(load "movimientos/torres.lisp")
(load "movimientos/alfiles.lisp")
(load "movimientos/damas.lisp")
(load "movimientos/rey.lisp")



;;; Funcion para ejecutar un movimiento sobre un tablero
;;; Esta funcion NO COMPRUEBA que la posicion de destino esta vacia, esa
;;; es tarea de valida-movimiento.
(defun ejecuta-movimiento (tablero movimiento)
  (ejecuta-movimiento-alpaso tablero movimiento)
  (ejecuta-movimiento-tablero tablero movimiento)
  (if (eq (tablero-posicion (tablero-blancas tablero)
			    (movimiento-desde movimiento))
	  '1)
      (progn
	(ejecuta-movimiento-blancas tablero movimiento)
	(when (posiciones-iguales (tablero-posicion-rey-blanco tablero)
				  (movimiento-desde movimiento))
	  (setf (tablero-posicion-rey-blanco tablero)
		(movimiento-hasta movimiento))
	  (when (not (tablero-rey-blanco-movido tablero))
	    (setf (tablero-rey-blanco-movido tablero) t)
	    ;; Comprobamos si es un enroque y, en caso positivo,
	    ;; movemos la torre correspondiente
	    (if (eq (tablero-posicion-inicial-blancas tablero) 'abajo)
		(cond ((posiciones-iguales
			(movimiento-hasta movimiento)
			(make-posicion :fila 7 :columna 6))
		       (ejecuta-movimiento
			tablero
			(make-movimiento
			 :desde (make-posicion :fila 7 :columna 7)
			 :hasta (make-posicion :fila 7 :columna 5))))
		      ((posiciones-iguales
			(movimiento-hasta movimiento)
			(make-posicion :fila 7 :columna 2))
		       (ejecuta-movimiento
			tablero
			(make-movimiento
			 :desde (make-posicion :fila 7 :columna 0)
			 :hasta (make-posicion :fila 7 :columna 3)))))
		(cond ((posiciones-iguales
			(movimiento-hasta movimiento)
			(make-posicion :fila 0 :columna 5))
		       (ejecuta-movimiento
			tablero
			(make-movimiento
			 :desde (make-posicion :fila 0 :columna 7)
			 :hasta (make-posicion :fila 0 :columna 4))))
		      ((posiciones-iguales
			(movimiento-hasta movimiento)
			(make-posicion :fila 0 :columna 1))
		       (ejecuta-movimiento
			tablero
			(make-movimiento
			 :desde (make-posicion :fila 0 :columna 0)
			 :hasta (make-posicion :fila 0 :columna 2))))))))
	  (dama-si-corona tablero movimiento))
      (progn
	(ejecuta-movimiento-negras tablero movimiento)
	(when (posiciones-iguales (tablero-posicion-rey-negro tablero)
				  (movimiento-desde movimiento))
	  (setf (tablero-posicion-rey-negro tablero)
		(movimiento-hasta movimiento))
	  (when (not (tablero-rey-negro-movido tablero))
	    (setf (tablero-rey-negro-movido tablero) t)
	    ;; Comprobamos si es un enroque y, en caso positivo,
	    ;; movemos la torre correspondiente
	    (if (eq (tablero-posicion-inicial-blancas tablero) 'arriba)
		(cond ((posiciones-iguales
			(movimiento-hasta movimiento)
			(make-posicion :fila 7 :columna 5))
		       (ejecuta-movimiento
			tablero
			(make-movimiento
			 :desde (make-posicion :fila 7 :columna 7)
			 :hasta (make-posicion :fila 7 :columna 4))))
		      ((posiciones-iguales
			(movimiento-hasta movimiento)
			(make-posicion :fila 7 :columna 1))
		       (ejecuta-movimiento
			tablero
			(make-movimiento
			 :desde (make-posicion :fila 7 :columna 0)
			 :hasta (make-posicion :fila 7 :columna 2)))))
		(cond ((posiciones-iguales
			(movimiento-hasta movimiento)
			(make-posicion :fila 0 :columna 6))
		       (ejecuta-movimiento
			tablero
			(make-movimiento
			 :desde (make-posicion :fila 0 :columna 7)
			 :hasta (make-posicion :fila 0 :columna 5))))
		      ((posiciones-iguales
			(movimiento-hasta movimiento)
			(make-posicion :fila 0 :columna 2))
		       (ejecuta-movimiento
			tablero
			(make-movimiento
			 :desde (make-posicion :fila 0 :columna 0)
			 :hasta (make-posicion :fila 0 :columna 1))))))))
	(dama-si-corona tablero movimiento)))
      tablero)

;;; Funcion que mueve una pieza en el tablero general de piezas
(defun ejecuta-movimiento-tablero (tablero movimiento)
  (setf (tablero-posicion (tablero-tablero tablero) (movimiento-hasta movimiento))
	(tablero-posicion (tablero-tablero tablero) (movimiento-desde movimiento)))
  (setf (tablero-posicion (tablero-tablero tablero) (movimiento-desde movimiento))
	'VV))

;;; Funcion que mueve una pieza en el tablero de blancas
(defun ejecuta-movimiento-blancas (tablero movimiento)
  (setf (tablero-posicion (tablero-blancas tablero) (movimiento-hasta movimiento)) '1)
  (setf (tablero-posicion (tablero-blancas tablero) (movimiento-desde movimiento)) '0)
  (when (eq (tablero-posicion (tablero-negras tablero) (movimiento-hasta movimiento)) '1)
    (setf (tablero-posicion (tablero-negras tablero) (movimiento-hasta movimiento)) '0)))

;;; Funcion que mueve una pieza en el tablero general de negras
(defun ejecuta-movimiento-negras (tablero movimiento)
  (setf (tablero-posicion (tablero-negras tablero)
			  (movimiento-hasta movimiento))
	'1)
  (setf (tablero-posicion (tablero-negras tablero)
			  (movimiento-desde movimiento))
	'0)
  (when (eq (tablero-posicion (tablero-blancas tablero)
			      (movimiento-hasta movimiento))
	    '1)
    (setf (tablero-posicion (tablero-blancas tablero)
			    (movimiento-hasta movimiento))
	  '0)))

(defun dama-si-corona (tablero movimiento)
  (cond ((and (or (eq (posicion-fila (movimiento-hasta movimiento)) '7)
		  (eq (posicion-fila (movimiento-hasta movimiento)) '0))
	      (eq (tablero-posicion (tablero-tablero tablero) (movimiento-hasta movimiento)) 'PB))
	 (setf (tablero-posicion (tablero-tablero tablero) (movimiento-hasta movimiento)) 'DB))
	((and (or (eq (posicion-fila (movimiento-hasta movimiento)) '7)
		  (eq (posicion-fila (movimiento-hasta movimiento)) '0))
	      (eq (tablero-posicion (tablero-tablero tablero) (movimiento-hasta movimiento)) 'PN))
	 (setf (tablero-posicion (tablero-tablero tablero) (movimiento-hasta movimiento)) 'DN))))

;;; Funcion que tiene en cuenta los arrays de gestion de peones
;;; "al paso" a la hora de mover
(defun ejecuta-movimiento-alpaso (tablero movimiento)
  (if (eq (tablero-posicion-inicial-blancas tablero) 'abajo)
      ;; Las blancas estan abajo

      (let ((el-tablero (tablero-tablero tablero))
	    (desde (movimiento-desde movimiento))
	    (hasta (movimiento-hasta movimiento)))
	(cond ((and (eq (tablero-posicion el-tablero desde) 'PN)
		    (eq (posicion-fila hasta) 5)
		    (eq (aref (tablero-blancas-alpaso tablero) (posicion-columna hasta)) '1))
	       ;; Se esta capturando un peon blanco en la posicion de al paso
	       (setf (aref el-tablero 4 (posicion-columna hasta)) 'VV)
	       (setf (aref (tablero-blancas tablero) 4 (posicion-columna hasta)) '0))
	      ((and (eq (tablero-posicion el-tablero desde) 'PB)
		    (eq (posicion-fila hasta) 2)
		    (eq (aref (tablero-negras-alpaso tablero) (posicion-columna hasta))	'1))
	       ;; Se esta capturando un peon negro en la posicion de al paso
	       (setf (aref el-tablero 3 (posicion-columna hasta)) 'VV)
	       (setf (aref (tablero-negras tablero) 3 (posicion-columna hasta)) '0)))

	(setf (tablero-blancas-alpaso tablero)
	      (make-array 8 :initial-contents '(0 0 0 0 0 0 0 0)))
	(setf (tablero-negras-alpaso tablero)
	      (make-array 8 :initial-contents '(0 0 0 0 0 0 0 0)))

	(cond ((eq (tablero-posicion el-tablero desde) 'PB)
	       ;; La pieza que se pretende mover es un peon blanco
	       (if (and (eq (posicion-fila desde) '6)
			(eq (posicion-fila hasta) '4))
		   ;; El peon esta en posicion inicial y se pretende hacer
		   ;; una salida doble
		   (setf (aref (tablero-blancas-alpaso tablero) (posicion-columna hasta)) '1)))
	      
	      ((eq (tablero-posicion el-tablero desde) 'PN)
	       ;; La pieza que se pretende mover es un peon negro
	       (if (and (eq (posicion-fila desde) '1)
			(eq (posicion-fila hasta) '3))
		   ;; El peon esta en posicion inicial y se pretende hacer
		   ;; una salida doble
		   (setf (aref (tablero-negras-alpaso tablero) (posicion-columna hasta)) '1)))))


      ;; Las blancas estan arriba
      (let ((el-tablero (tablero-tablero tablero))
	    (desde (movimiento-desde movimiento))
	    (hasta (movimiento-hasta movimiento)))
	
	(cond ((and (eq (tablero-posicion el-tablero desde) 'PN)
		    (eq (posicion-fila hasta) 2)
		    (eq (aref (tablero-blancas-alpaso tablero) (posicion-columna hasta)) '1))
	       ;; Se esta capturando un peon blanco en la posicion de al paso
	       (setf (aref el-tablero 3 (posicion-columna hasta)) 'VV)
	       (setf (aref (tablero-blancas tablero) 3 (posicion-columna hasta)) '0))
	      ((and (eq (tablero-posicion el-tablero desde) 'PB)
		    (eq (posicion-fila hasta) 5)
		    (eq (aref (tablero-negras-alpaso tablero) (posicion-columna hasta)) '1))
	       ;; Se esta capturando un peon negro en la posicion de al paso
	       (setf (aref el-tablero 4 (posicion-columna hasta)) 'VV)
	       (setf (aref (tablero-negras tablero) 4 (posicion-columna hasta)) '0)))

	(setf (tablero-blancas-alpaso tablero)
	      (make-array 8 :initial-contents '(0 0 0 0 0 0 0 0)))
	(setf (tablero-negras-alpaso tablero)
	      (make-array 8 :initial-contents '(0 0 0 0 0 0 0 0)))


	(cond ((eq (tablero-posicion el-tablero desde) 'PB)
	       ;; La pieza que se pretende mover es un peon blanco
	       (if (and (eq (posicion-fila desde) '1)
			(eq (posicion-fila hasta) '3))
		   ;; El peon esta en posicion inicial y se pretende hacer
		   ;; una salida doble
		   (setf (aref (tablero-blancas-alpaso tablero) (posicion-columna hasta)) '1)))
	    
	      ((eq (aref el-tablero (posicion-fila desde) (posicion-columna desde)) 'PN)
	       ;; La pieza que se pretende mover es un peon negro
	       (if (and (eq (posicion-fila desde) '6)
			(eq (posicion-fila hasta) '4))
		   ;; El peon esta en posicion inicial y se pretende hacer
		   ;; una salida doble
		   (setf (aref (tablero-negras-alpaso tablero) (posicion-columna hasta)) '1)))))))

;; REVISION_1 - finalizada
(defun sucesores-pieza (tablero posicion &key enroque)
  (let ((sucesores '())
	(pieza (tablero-posicion (tablero-tablero tablero) posicion)))
    (cond ((or (eq pieza 'PB) (eq pieza 'PN))
	   (setf sucesores (posibles-peon tablero posicion)))
	  ((or (eq pieza 'CB) (eq pieza 'CN))
	   (setf sucesores (posibles-caballo tablero posicion)))
	  ((or (eq pieza 'TB) (eq pieza 'TN))
	   (setf sucesores (posibles-torre tablero posicion)))
	  ((or (eq pieza 'AB) (eq pieza 'AN))
	   (setf sucesores (posibles-alfil tablero posicion)))
	  ((or (eq pieza 'DB) (eq pieza 'DN))
	   (setf sucesores (posibles-dama tablero posicion)))
	  ((or (eq pieza 'RB) (eq pieza 'RN))
	   (setf sucesores
		 (if (eq enroque 'sin-enroque)
		     (posibles-rey-sin-enroque tablero posicion)
		     (posibles-rey tablero posicion)))))
    (mapcar #'(lambda (hasta)
		(make-movimiento :desde posicion :hasta hasta)) sucesores)))

;; REVISION_1 - finalizada
(defun sucesores (tablero color &key enroque)
  (let ((tablero-color (if (blancop color)
			   (tablero-blancas tablero)
			   (tablero-negras tablero)))
	(sucesores '()))
    (dotimes (fila 8 sucesores)
      (dotimes (columna 8)
	(when (color-ocupada-p tablero-color fila columna)
	  (let ((pos-actual (make-posicion :fila fila
					   :columna columna)))
	    (setf sucesores (nconc sucesores
				   (sucesores-pieza
				    tablero
				    pos-actual
				    :enroque enroque)))))))))

;;; Para saber si una posicion esta amenazada para un color
(defun posicion-amenazada (la-mesa la-posicion el-color)
  (member la-posicion
	  (mapcar #'movimiento-hasta
		  (sucesores la-mesa
			     (cambia-color el-color)
			     :enroque 'sin-enroque))
	  :test #'posiciones-iguales))

(defun valido (tablero movimiento color)
  (let ((pieza (if (eq color 'blanco) 'RB 'RN)))
    (and (member movimiento (sucesores tablero color)
		 :test #'movimientos-iguales)
	 (valido-rey tablero movimiento color))))

(defun valido-rey (tablero movimiento color)
  (let ((nuevo-tablero (copia-tablero tablero)))
    (ejecuta-movimiento nuevo-tablero movimiento)
    (null (rey-amenazado nuevo-tablero color))))

(defun rey-amenazado (tablero color)
  (posicion-amenazada tablero
		      (if (eq color 'blanco)
			  (tablero-posicion-rey-blanco tablero)
			  (tablero-posicion-rey-negro tablero))
		      color))

(defun jaque-mate (la-mesa el-jugador)
  (and
   ;; El rey esta amenazado
   (rey-amenazado la-mesa (jugador-color el-jugador))
   ;; Para todos los sucesores del rey, el rey sigue amenazado
   (let ((la-posicion (if (eq (jugador-color el-jugador) 'blanco)
			  (tablero-posicion-rey-blanco la-mesa)
			  (tablero-posicion-rey-negro la-mesa)))
	 (algun-movimiento-posible))
     (dolist (un-movimiento
	       (sucesores la-mesa (jugador-color el-jugador) :enroque 'sin-enroque)
	      (not algun-movimiento-posible))
       ;; Si se puede mover a un-movimiento, ponemos
       ;; algun-movimiento-posible a T
       (let* ((nueva-mesa (ejecuta-movimiento (copia-tablero la-mesa)
					     un-movimiento))
	      (nueva-posicion-rey (if (eq (jugador-color el-jugador) 'blanco)
				      (tablero-posicion-rey-blanco nueva-mesa)
				      (tablero-posicion-rey-negro nueva-mesa))))
	 (when (not (posicion-amenazada nueva-mesa
					nueva-posicion-rey
					(jugador-color el-jugador)))
	   (setf algun-movimiento-posible t)))))))
