;;; -*- encoding: utf-8 -*-
;;;
;;; peones.lisp
;;; Movimiento de los peones (Si es el primer movimiento puede avanzar 2 cuadros.ataque horizontal avanzando 1 cuadro)


;;; REVISION_1: finalizada
;;; Lugares a los que se puede mover un peon sin atacar.
(defun posiciones-libres-peon (tablero posicion)
  (let ((posiciones '()))
    ;; Las blancas estan abajo
    (if (eq (tablero-posicion-inicial-blancas tablero) 'abajo)
	;; El peon en cuestion es blanco
	(if (eq (tablero-posicion (tablero-tablero tablero) posicion) 'PB)
	    (progn
	      ;; NO estamos en el borde superior
	      (when (not (= (posicion-fila posicion) 0))
		(anhade-posicion posiciones
		    (1- (posicion-fila posicion))
		    (posicion-columna posicion)))
	      ;; Estamos en la posicion inicial: podemos avanzar dos
	      ;; posiciones si tenemos el camino libre.
	      (when (and (= (posicion-fila posicion) 6)
			 (eq (aref (tablero-tablero tablero)
				   5
				   (posicion-columna posicion))
			     'VV))
		(anhade-posicion posiciones 4 (posicion-columna posicion))))
	    ;; El peon en cuestion es negro
	    (progn
	      ;; No estamos en el borde inferior
	      (when (not (= (posicion-fila posicion) 7))
		(anhade-posicion posiciones
		    (1+ (posicion-fila posicion))
		    (posicion-columna posicion)))
	      ;; Estamos en la posicion inicial: podemos avanzar dos
	      ;; posiciones si tenemos el camino libre.
	      (when (and (= (posicion-fila posicion) 1)
			 (eq (aref (tablero-tablero tablero)
				   2
				   (posicion-columna posicion))
			     'VV))
		(anhade-posicion posiciones 3 (posicion-columna posicion)))))
	;; Las blancas eston arriba
	;; El peon en cuestion es blanco
	(if (eq (tablero-posicion (tablero-tablero tablero) posicion) 'PB)
	    (progn
	      ;; No estamos en el borde inferior
	      (when (not (= (posicion-fila posicion) 7))
		(anhade-posicion posiciones
		    (1+ (posicion-fila posicion))
		    (posicion-columna posicion)))
	      ;; Estamos en la posicion inicial: podemos avanzar dos
	      ;; posiciones si tenemos el camino libre.
	      (when (and (= (posicion-fila posicion) 1)
			 (eq (aref (tablero-tablero tablero)
				   2
				   (posicion-columna posicion))
			     'VV))
		(anhade-posicion posiciones 3 (posicion-columna posicion))))
	    ;; El peon en cuestion es negro
	    (progn
	      ;; No estamos en el borde superior
	      (when (not (= (posicion-fila posicion) 0))
		(anhade-posicion posiciones
		    (1- (posicion-fila posicion))
		    (posicion-columna posicion)))
	      ;; Estamos en la posicion inicial: podemos avanzar dos
	      ;; posiciones si tenemos el camino libre.
	      (when (and (= (posicion-fila posicion) 6)
			 (eq (aref (tablero-tablero tablero)
				   5
				   (posicion-columna posicion))
			     'VV))
		(aade-posicion posiciones 4 (posicion-columna posicion))))))
    posiciones))




;;; REVISION_1: finalizada
;;; Lugares a los que se puede mover un peon al atacar.
(defun posiciones-ataque-peon (tablero posicion)
  (let ((posiciones '()))
    ;; Las blancas estan abajo
    (if (eq (tablero-posicion-inicial-blancas tablero) 'abajo)
	;; El peon en cuestion es blanco
	(if (eq (tablero-posicion (tablero-tablero tablero) posicion) 'PB)
	    ;; NO estamos en el borde superior
	    (when (not (= (posicion-fila posicion) 0))
	      ;; No estamos en el borde izquierdo
	      (when (not (= (posicion-columna posicion) 0))
		(anhade-posicion posiciones
		    (1- (posicion-fila posicion))
		    (1- (posicion-columna posicion))))
	      ;; No estamos en el borde derecho
	      (when (not (= (posicion-columna posicion) 7))
		(anhade-posicion posiciones
		    (1- (posicion-fila posicion))
		    (1+ (posicion-columna posicion)))))
	    ;; El peon en cuestion es negro
	    ;; No estamos en el borde inferior
	    (when (not (= (posicion-fila posicion) 7))
	      ;; No estamos en el borde izquierdo
	      (when (not (= (posicion-columna posicion) 0))
		(anhade-posicion posiciones
		    (1+ (posicion-fila posicion))
		    (1- (posicion-columna posicion))))
	      ;; No estamos en el borde derecho
	      (when (not (= (posicion-columna posicion) 7))
		(anhade-posicion posiciones
		    (1+ (posicion-fila posicion))
		    (1+ (posicion-columna posicion))))))
	;; Las blancas estan arriba
	;; El peon en cuestion es blanco
	(if (eq (tablero-posicion (tablero-tablero tablero) posicion) 'PB)
	    ;; No estamos en el borde inferior
	    (when (not (= (posicion-fila posicion) 7))
	      ;; No estamos en el borde izquierdo
	      (when (not (= (posicion-columna posicion) 0))
		(anhade-posicion posiciones
		    (1+ (posicion-fila posicion))
		    (1- (posicion-columna posicion))))
	      ;; No estamos en el borde derecho
	      (when (not (= (posicion-columna posicion) 7))
		(anhade-posicion posiciones
		    (1+ (posicion-fila posicion))
		    (1+ (posicion-columna posicion)))))
	    ;; El peon en cuestion es negro
	    ;; No estamos en el borde superior
	    (when (not (= (posicion-fila posicion) 0))
	      ;; No estamos en el borde izquierdo
	      (when (not (= (posicion-columna posicion) 0))
		(anhade-posicion posiciones
		    (1- (posicion-fila posicion))
		    (1- (posicion-columna posicion))))
	      ;; No estamos en el borde derecho
	      (when (not (= (posicion-columna posicion) 7))
		(anhade-posicion posiciones
		    (1- (posicion-fila posicion))
		    (1+ (posicion-columna posicion)))))))
    posiciones))




;;; REVISION_1: finalizada
;;; Posiciones a las que es posible que un peon se desplace
(defun posibles-libres-peon (tablero posicion posiciones)
  (dolist (una-posicion (posiciones-libres-peon tablero posicion))
    (when (eq (tablero-posicion (tablero-tablero tablero) una-posicion) 'VV)
      (setf posiciones (cons una-posicion posiciones))))
  posiciones)




;;; REVISION_1: finalizada
;;; Posiciones a las que un peon determinado puede atacar.
;;; NOTA: Se tienen en cuenta las capturas al paso.
(defun posibles-ataque-peon (tablero posicion posiciones)
  (if (eq (tablero-posicion (tablero-tablero tablero) posicion) 'PB)
      ;; El peon es blanco
      (dolist (una-posicion (posiciones-ataque-peon tablero posicion))
	(cond ((color-ocupada-p (tablero-negras tablero)
				(posicion-fila una-posicion)
				(posicion-columna una-posicion))
	       ;; La posicion destino esta ocupada por una pieza negra.
	       (setf posiciones (cons una-posicion posiciones)))
	      ((and (eq (posicion-fila una-posicion) 2)
		    (eq (tablero-posicion-inicial-blancas tablero) 'abajo)
		    (eq (aref (tablero-negras-alpaso tablero)
			      (posicion-columna una-posicion))
			'1))
	       ;; Las blancas estan abajo, el peon se mueve a la fila de las
	       ;; capturas al paso y, efectivamente, hay un peon susceptible
	       ;; de ser comido al paso.
	       (setf posiciones (cons una-posicion posiciones)))
	      ((and (eq (posicion-fila una-posicion) 5)
		    (eq (tablero-posicion-inicial-blancas tablero) 'arriba)
		    (eq (aref (tablero-negras-alpaso tablero)
			      (posicion-columna una-posicion))
			'1))
	       ;; Las blancas estan arriba, el peon se mueve a la fila de las
	       ;; capturas al paso y, efectivamente, hay un peon susceptible
	       ;; de ser comido al paso.
	       (setf posiciones (cons una-posicion posiciones)))))
      
      ;; El peon es negro
      (dolist (una-posicion (posiciones-ataque-peon tablero posicion))
	(cond ((color-ocupada-p (tablero-blancas tablero)
				(posicion-fila una-posicion)
				(posicion-columna una-posicion))
	       ;; La posicion destino esta ocupada por una pieza blanca.
	       (setf posiciones (cons una-posicion posiciones)))
	      ((and (eq (posicion-fila una-posicion) 5)
		    (eq (tablero-posicion-inicial-blancas tablero) 'abajo)
		    (eq (aref (tablero-blancas-alpaso tablero)
			      (posicion-columna una-posicion))
			'1))
	       ;; Las blancas estan abajo, el peon se mueve a la fila de las
	       ;; capturas al paso y, efectivamente, hay un peon susceptible
	       ;; de ser comido al paso.
	       (setf posiciones (cons una-posicion posiciones)))
	      ((and (eq (posicion-fila una-posicion) 2)
		    (eq (tablero-posicion-inicial-blancas tablero) 'arriba)
		    (eq (aref (tablero-blancas-alpaso tablero)
			      (posicion-columna una-posicion))
			'1))
	       ;; Las blancas estan arriba, el peon se mueve a la fila de las
	       ;; capturas al paso y, efectivamente, hay un peon susceptible
	       ;; de ser comido al paso.
	       (setf posiciones (cons una-posicion posiciones))))))
  posiciones)





(defun posibles-peon (tablero posicion)
  (incf *llamadas*)
  (let ((posiciones '()))
    (setf posiciones (posibles-ataque-peon tablero posicion posiciones))
    (setf posiciones (posibles-libres-peon tablero posicion posiciones))
    posiciones))
