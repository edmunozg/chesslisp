;;; -*- encoding: utf-8 -*-
;;;
;;; rey.lisp
;;; Movimientos de los reyes (Ataque y movimientos avanzando una celda a su alrededores)
;;; Es la pieza más debil pero la más importante
				  

(defun genera-presuntas-posiciones-rey (posicion)
  (let ((fila (posicion-fila posicion))
	(columna (posicion-columna posicion))
	(presuntas-posiciones '()))
    (when (> fila 0) ; Doce
      (anhade-posicion presuntas-posiciones (- fila 1) columna))
    (when (and (> fila 0) (< columna 7)) ; Una y media
      (anhade-posicion presuntas-posiciones (- fila 1) (1+ columna)))
    (when (< columna 7) ; Tres
      (anhade-posicion presuntas-posiciones fila (1+ columna)))
    (when (and (< fila 7) (< columna 7)) ; Cuatro y media
      (anhade-posicion presuntas-posiciones (1+ fila) (1+ columna)))
    (when (< fila 7) ; Seis
      (anhade-posicion presuntas-posiciones (1+ fila) columna))
    (when (and (< fila 7) (> columna 0)) ; Siete y media
      (anhade-posicion presuntas-posiciones (1+ fila) (- columna 1)))
    (when (> columna 0) ; Nueve
      (anhade-posicion presuntas-posiciones fila (- columna 1)))
    (when (and (> fila 0) (> columna 0)) ; Diez y media
      (anhade-posicion presuntas-posiciones (- fila 1) (- columna 1)))
    presuntas-posiciones))





(defun posicion-vacia (tablero posicion)
  (eq (aref (tablero-tablero tablero)
	    (posicion-fila posicion)
	    (posicion-columna posicion))
      'VV))

(defun posicion-ocupada (tablero posicion funcion)
  (eq (aref (funcall funcion tablero)
	    (posicion-fila posicion)
	    (posicion-columna posicion))
      '1))



(defun posibles-rey-sin-enroque (tablero posicion)
  (let ((funcion-contrarias (if (eq (aref (tablero-tablero tablero)
					  (posicion-fila posicion)
					  (posicion-columna posicion))
				    'RB)
				#'tablero-negras
			      #'tablero-blancas))
	(color (if (eq (aref (tablero-tablero tablero)
			     (posicion-fila posicion)
			     (posicion-columna posicion))
		       'RB)
		   'blanco
		   'negro))
	(presuntas-posiciones
	 (genera-presuntas-posiciones-rey posicion))
	(posiciones '()))
    (dolist (p presuntas-posiciones)
      (if (or (posicion-vacia tablero p)
 	      (posicion-ocupada tablero p funcion-contrarias))
 	  (setf posiciones (cons p posiciones))))
    posiciones))


(defun posibles-rey (tablero posicion)
  (let ((color (if (eq (aref (tablero-tablero tablero)
			     (posicion-fila posicion)
			     (posicion-columna posicion))
		       'RB)
		   'blanco
		   'negro))
	(posiciones (posibles-rey-sin-enroque tablero posicion)))
    (setf posiciones (append posiciones (enroca-corto-si-posible tablero posiciones color)))
    (setf posiciones (append posiciones (enroca-largo-si-posible tablero posiciones color)))
    posiciones))



(defun enroca-corto-si-posible (tablero lista color)
  (cond ((and (eq color 'blanco)
 	      (eq (tablero-posicion-inicial-blancas tablero) 'abajo))
 	 (when (and (not (tablero-rey-blanco-movido tablero))
 		    (posicion-vacia tablero
 				    (make-posicion :fila 7 :columna 5))
 		    (posicion-vacia tablero
 				    (make-posicion :fila 7 :columna 6))
 		    (eq (tablero-posicion (tablero-tablero tablero)
 					  (make-posicion :fila 7
 							 :columna 7))
 			'TB)
 		    (not (rey-amenazado tablero color)))
 	   (anhade-posicion lista 7 6)))
	((and (eq color 'blanco)
	      (eq (tablero-posicion-inicial-blancas tablero) 'arriba))
	 (when (and (not (tablero-rey-blanco-movido tablero))
		    (posicion-vacia tablero
				    (make-posicion :fila 0 :columna 2))
		    (posicion-vacia tablero
				    (make-posicion :fila 0 :columna 1))
		    (eq (tablero-posicion (tablero-tablero tablero)
					  (make-posicion :fila 0
							 :columna 0))
			'TB)
		    (not (rey-amenazado tablero color)))
	   (anhade-posicion lista 0 1)))
	((and (eq color 'negro)
	      (eq (tablero-posicion-inicial-blancas tablero) 'abajo))
	 (when (and (not (tablero-rey-negro-movido tablero))
		    (posicion-vacia tablero
				    (make-posicion :fila 0 :columna 5))
		    (posicion-vacia tablero
				    (make-posicion :fila 0 :columna 6))
		    (eq (tablero-posicion (tablero-tablero tablero)
					  (make-posicion :fila 0
							 :columna 7))
			'TN)
		    (not (rey-amenazado tablero color)))
	   (anhade-posicion lista 0 6)))
	((and (eq color 'negro)
	      (eq (tablero-posicion-inicial-blancas tablero) 'arriba))
	 (when (and (not (tablero-rey-negro-movido tablero))
		    (posicion-vacia tablero
				    (make-posicion :fila 7 :columna 2))
		    (posicion-vacia tablero
				    (make-posicion :fila 7 :columna 1))
		    (eq (tablero-posicion (tablero-tablero tablero)
					  (make-posicion :fila 7
							 :columna 0))
			'TN)
		    (not (rey-amenazado tablero color)))
	   (anhade-posicion lista 7 1)))))


(defun enroca-largo-si-posible (tablero lista color)
  (cond ((and (eq color 'blanco)
 	      (eq (tablero-posicion-inicial-blancas tablero) 'abajo))
 	 (when (and (not (tablero-rey-blanco-movido tablero))
 		    (posicion-vacia tablero
 				    (make-posicion :fila 7 :columna 2))
 		    (posicion-vacia tablero
 				    (make-posicion :fila 7 :columna 3))
 		    (eq (tablero-posicion (tablero-tablero tablero)
 					  (make-posicion :fila 7
 							 :columna 0))
 			'TB)
 		    (not (rey-amenazado tablero color)))
 	   (anhade-posicion lista 7 2)))
	((and (eq color 'blanco)
	      (eq (tablero-posicion-inicial-blancas tablero) 'arriba))
	 (when (and (not (tablero-rey-blanco-movido tablero))
		    (posicion-vacia tablero
				    (make-posicion :fila 0 :columna 5))
		    (posicion-vacia tablero
				    (make-posicion :fila 0 :columna 4))
		    (eq (tablero-posicion (tablero-tablero tablero)
					  (make-posicion :fila 0
							 :columna 7))
			'TB)
		    (not (rey-amenazado tablero color)))
	   (anhade-posicion lista 0 5)))
	((and (eq color 'negro)
	      (eq (tablero-posicion-inicial-blancas tablero) 'abajo))
	 (when (and (not (tablero-rey-negro-movido tablero))
		    (posicion-vacia tablero
				    (make-posicion :fila 0 :columna 2))
		    (posicion-vacia tablero
				    (make-posicion :fila 0 :columna 3))
		    (eq (tablero-posicion (tablero-tablero tablero)
					  (make-posicion :fila 0
							 :columna 0))
			'TN)
		    (not (rey-amenazado tablero color)))
	   (anhade-posicion lista 0 2)))
	((and (eq color 'negro)
	      (eq (tablero-posicion-inicial-blancas tablero) 'arriba))
	 (when (and (not (tablero-rey-negro-movido tablero))
		    (posicion-vacia tablero
				    (make-posicion :fila 7 :columna 5))
		    (posicion-vacia tablero
				    (make-posicion :fila 7 :columna 4))
		    (eq (tablero-posicion (tablero-tablero tablero)
					  (make-posicion :fila 7
							 :columna 7))
			'TN)
		    (not (rey-amenazado tablero color)))
	   (anhade-posicion lista 7 5)))))
