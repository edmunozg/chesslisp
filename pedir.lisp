;;; -*- encoding: utf-8 -*-
;;;
;;; es.lisp
;;; Entrada/Salida

;;; Tabla hash con las descripciones de las fichas
(setf piezas (make-hash-table))
(setf (gethash 'TN piezas) "TN")
(setf (gethash 'TB piezas) "TB")
(setf (gethash 'CN piezas) "CN")
(setf (gethash 'CB piezas) "CB")
(setf (gethash 'AN piezas) "AN")
(setf (gethash 'AB piezas) "AB")
(setf (gethash 'RN piezas) "RN")
(setf (gethash 'RB piezas) "RB")
(setf (gethash 'DN piezas) "DN")
(setf (gethash 'DB piezas) "DB")
(setf (gethash 'PN piezas) "PN")
(setf (gethash 'PB piezas) "PB")
(setf (gethash 'VV piezas) "..")


;;; Presentamos el tablero al usuario
(defun muestra-tablero (tablero)
	(format t "~%     A    B    C    D    E    F    G    H~%~%")
	(dotimes (fila 8)
		(format t " ~A  " (- 8 fila))
		(dotimes (columna 8)
			(format t "~A   " (gethash (aref (tablero-tablero tablero) fila columna) piezas))
		)
		(format t "~%~%")
    )
)


;;; Preguntamos por un movimiento al usuario
(defun lee-movimiento ()
	(let (desde-fila desde-columna hasta-fila hasta columna)
	    (loop
	    	(when 
	    		(and
	    			(not (null desde-fila))
	    			(not (null desde-columna))
	    			(not (null hasta-fila))
	    			(not (null hasta-columna)))
		 		(return)
		 	)
	       	(format t "Movimiento ficha Ej:(b1 c3): ")
	       	(let
	       		(
	       			(mov1 (read))
		     		(mov2 (read))
		     	)
			    (ignore-errors
			    	(when (= 2 (length (string mov1)))
			    		(setf desde-fila (digit-char-p (aref (string mov1) 1)))
			    		(setf desde-columna (position (aref (string mov1) 0) "ABCDEFGH"
			    			:test (function string-equal)))
			    	)
				   	(when (= 2 (length (string mov2)))
				   		(setf hasta-fila (digit-char-p (aref (string mov2) 1)))
				     	(setf hasta-columna (position (aref (string mov2) 0) "ABCDEFGH"
				     		:test (function string-equal)))
				    )
				)
			)
	    )
		;;(format t "~A ~A ~A ~A" desde-fila desde-columna hasta-fila hasta-columna)
	    (make-movimiento
	    	:desde (make-posicion :fila (- 8 desde-fila)
			:columna desde-columna)
			:hasta (make-posicion :fila (- 8 hasta-fila)
			:columna hasta-columna)
		)
	)
)
