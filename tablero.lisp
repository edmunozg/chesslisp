;;; -*- encoding: utf-8 -*-
;;;
;;; datos.lisp
;;; Definicion de datos y funciones para su manipulacion


;;; Tipo de datos principal del programa
(defstruct tablero
  (tablero)
  (blancas)
  (negras)
  (blancas-alpaso)
  (negras-alpaso)
  (posicion-inicial-blancas)
  (posicion-rey-blanco)
  (posicion-rey-negro)
  (rey-blanco-movido)
  (rey-negro-movido))

;;; Jugador
(defstruct jugador
  (tipo) 	;; 'auto, 'manual
  (color)) 	;; 'blanco, 'negro

(defun copia-tablero (tablero)
  (make-tablero :tablero (copia-array-tablero (tablero-tablero tablero))
		:blancas (copia-array-tablero (tablero-blancas tablero))
		:negras (copia-array-tablero (tablero-negras tablero))
		:blancas-alpaso (copia-array-fila
				 (tablero-blancas-alpaso tablero))
		:negras-alpaso (copia-array-fila
				(tablero-negras-alpaso tablero))
		:posicion-inicial-blancas
		(tablero-posicion-inicial-blancas tablero)
		:posicion-rey-blanco
		(copy-posicion (tablero-posicion-rey-blanco tablero))
		:posicion-rey-negro
		(copy-posicion (tablero-posicion-rey-negro tablero))
		:rey-blanco-movido (tablero-rey-blanco-movido tablero)
		:rey-negro-movido (tablero-rey-negro-movido tablero)))

(defun copia-array-tablero (array-tablero)
  (let ((nuevo-array-tablero (make-array '(8 8))))
    (dotimes (fila 8)
      (dotimes (columna 8)
	(setf (aref nuevo-array-tablero fila columna)
	      (aref array-tablero fila columna))))
    nuevo-array-tablero))

(defun copia-array-fila (array-fila)
  (let ((nuevo-array-fila (make-array 8)))
    (dotimes (i 8 nuevo-array-fila)
      (setf (aref nuevo-array-fila i)
	    (aref array-fila i)))))


(defun cambia-color (color)
  (if (eq color 'blanco) 'negro 'blanco))

(defun blancop (color)
  (eq color 'blanco))

;;; Esta ocupada por un color una posicion determinada?
(defun color-ocupada-p (tablero-color fila columna)
  (eq (aref tablero-color fila columna ) '1))

;;; Una posicion cualquiera en el tablero, 0-7 x 2
(defstruct posicion
  (fila)
  (columna))

(defun posiciones-iguales (pos1 pos2)
  (and (eq (posicion-fila pos1) (posicion-fila pos2))
       (eq (posicion-columna pos1) (posicion-columna pos2))))


;;; Crea una nueva posicion a partir de la fila y columna pasadas
;;; como parametros y la anhade a la lista.
(defmacro anhade-posicion (lista-posiciones fila columna)
  `(setf ,lista-posiciones (cons (make-posicion
				  :fila ,fila
				  :columna ,columna)
			    ,lista-posiciones)))


;;; Un movimiento, posicion inicial y posicion final
(defstruct movimiento
  (desde)
  (hasta))

(defun movimientos-iguales (mov1 mov2)
  (and (posiciones-iguales (movimiento-desde mov1) (movimiento-desde mov2))
       (posiciones-iguales (movimiento-hasta mov1) (movimiento-hasta mov2))))


;;; Abreviamos un metodo muy utilizado: la celda de un vector 8x8
(defmacro tablero-posicion (tablero posicion)
  `(aref ,tablero (posicion-fila ,posicion) (posicion-columna ,posicion)))



;;; Funcion para crear un tablero inicial con las blancas abajo
(defun crear-tablero-inicial-blancas-abajo ()
  (make-tablero :tablero (make-array '(8 8)
				     :initial-contents
				     '((TN CN AN DN RN AN CN TN)
				       (PN PN PN PN PN PN PN PN)
				       (VV VV VV VV VV VV VV VV)
				       (VV VV VV VV VV VV VV VV)
				       (VV VV VV VV VV VV VV VV)
				       (VV VV VV VV VV VV VV VV)
				       (PB PB PB PB PB PB PB PB)
				       (TB CB AB DB RB AB CB TB)))
		:blancas (make-array '(8 8)
				     :initial-contents
				     '((0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (1 1 1 1 1 1 1 1)
				       (1 1 1 1 1 1 1 1)))
		:negras (make-array '(8 8)
				     :initial-contents
				     '((1 1 1 1 1 1 1 1)
				       (1 1 1 1 1 1 1 1)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)))
		:blancas-alpaso (make-array
				 8
				 :initial-contents '(0 0 0 0 0 0 0 0))
		:negras-alpaso (make-array
				 8
				 :initial-contents '(0 0 0 0 0 0 0 0))
		:posicion-inicial-blancas 'abajo
		:posicion-rey-blanco (make-posicion :fila 7 :columna 4)
		:posicion-rey-negro (make-posicion :fila 0 :columna 4)
		:rey-blanco-movido nil
		:rey-negro-movido nil))


;;; Funcion para crear un tablero inicial con las blancas arriba
(defun crear-tablero-inicial-blancas-arriba ()
  (make-tablero :tablero(make-array '(8 8)
				    :initial-contents
				    '((TB CB AB RB DB AB CB TB)
				      (PB PB PB PB PB PB PB PB)
				      (VV VV VV VV VV VV VV VV)
				      (VV VV VV VV VV VV VV VV)
				      (VV VV VV VV VV VV VV VV)
				      (VV VV VV VV VV VV VV VV)
				      (PN PN PN PN PN PN PN PN)
				      (TN CN AN RN DN AN CN TN)))
		:blancas (make-array '(8 8)
				     :initial-contents
				     '((1 1 1 1 1 1 1 1)
				       (1 1 1 1 1 1 1 1)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)))
		:negras (make-array '(8 8)
				     :initial-contents
				     '((0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (1 1 1 1 1 1 1 1)
				       (1 1 1 1 1 1 1 1)))
		:blancas-alpaso (make-array
				 8
				 :initial-contents '(0 0 0 0 0 0 0 0))
		:negras-alpaso (make-array
				 8
				 :initial-contents '(0 0 0 0 0 0 0 0))
		:posicion-inicial-blancas 'arriba
		:posicion-rey-blanco (make-posicion :fila 0 :columna 3)
		:posicion-rey-negro (make-posicion :fila 7 :columna 3)
		:rey-blanco-movido nil
		:rey-negro-movido nil))


;;; Funcion para crear un tablero inicial de prueba
(defun crear-tablero-inicial-pruebas ()
  (make-tablero :tablero (make-array '(8 8)
				     :initial-contents
				     '((VV VV VV VV VV VV RN VV)
				       (VV VV DB VV VV VV VV VV)
				       (VV VV VV VV RB VV VV VV)
				       (VV VV VV VV VV VV VV PN)
				       (VV VV VV VV VV VV VV PB)
				       (VV VV VV VV VV VV VV VV)
				       (VV VV VV VV VV PB VV VV)
				       (VV VV VV VV VV VV VV VV)))
		:blancas (make-array '(8 8)
				     :initial-contents
				     '((0 0 0 0 0 0 0 0)
				       (0 0 1 0 0 0 0 0)
				       (0 0 0 0 1 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 1)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 1 0 0)
				       (0 0 0 0 0 0 0 0)))
		:negras (make-array '(8 8)
				     :initial-contents
				     '((0 0 0 0 0 0 1 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 1)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)
				       (0 0 0 0 0 0 0 0)))
		:blancas-alpaso (make-array
				 8
				 :initial-contents '(0 0 0 0 0 0 0 0))
		:negras-alpaso (make-array
				 8
				 :initial-contents '(0 0 0 0 0 0 0 0))
		:posicion-inicial-blancas 'abajo
		:posicion-rey-blanco (make-posicion :fila 2 :columna 4)
		:posicion-rey-negro (make-posicion :fila 0 :columna 6)
		:rey-blanco-movido t
		:rey-negro-movido t))
