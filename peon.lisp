;;; -*- encoding: utf-8 -*-
;;;
;;; Movimiento del peon
;;; Esteban Muñoz
;;; Marco Mendoza



(defun free-pos-pawn (board pos)
	"Lugares a los que un peón puede moverse sin atacar"
	(let ((positions '()))
		; Piezas blancas en la parte inferior
		(if (eq (board-whites-initial-pos board) 'bottom)
			; El peón es de color blanco
			(if (eq (board-pos (board-board board) pos) 'PB)
			    (progn
			    	; No estamos en el borde superior
			      	(when 
			      		(not (= (pos-row pos) 0))
			      		(add-pos positions
			      			(1- (pos-row pos))
			      			(pos-col pos)
			      		)
			      	)
			      	; Estamos en la posición inicial: podemos avanzar de dos
			      	; celdas si el camino está despejado
			      	(when
			      		(and
			      			(= (pos-row pos) 6) ; (objeto.pos-row = pos) == 6
			      			(eq (aref (board-board board) 5 (pos-col pos)) ; pregunto si board[x][y] == VV
			      				'VV
			      			)
			      		)					
						(add-pos positions 4 (pos-col pos))
					)
			    )
			    ; El peón es negro
			    (progn
			    	; No estamos en el borde inferior
			    	(when
			    		(not (= (pos-row pos) 7))
						(add-pos positions
							(1+ (pos-row pos))
							(pos-col pos)
						)
					)
				    ; Estamos en la posicion inicial podemos avanzar dos espacios
				    ; Celdas despejadas
				    (when
				    	(and
				    		(= (pos-row pos) 1)
							(eq
								(aref (board-board board) 2 (pos-col pos))
						     	'VV
						    )
						)
						(add-pos positions 3 (pos-col pos))
					)
			    )
			)
			; Piezas blancas en la parte superior
			; Peon blanco
			(if (eq (board-pos (board-board board) pos) 'PB)
		    	(progn
		      		; No estamos en el borde inferior
		      		(when
		      			(not (= (pos-row pos) 7))
						(add-pos positions
						    (1+ (pos-row pos))
						    (pos-col pos))
					)
		      		; Estamos en la posicion inicial podemos avanzar dos espacios
		      		; Celdas despejadas
		      		(when
		      			(and
		      				(= (pos-row pos) 1)
					 		(eq 
					 			(aref (board-board board) 2 (pos-col pos))
					     		'VV
					     	)
				 		)
						(add-pos positions 3 (pos-col pos))
					)
		      	)
			    ; Peon negro
			    (progn
			    	; No estamos en el borde superior
			      	(when
			      		(not (= (pos-row pos) 0))
						(add-pos positions
							(1- (pos-row pos))
							(pos-col pos)
						)
				    )
			      	; Estamos en la posicion inicial podemos avanzar dos espacios
			      	; Celdas despejadas
			      	(when
			      		(and 
			      			(= (pos-row pos) 6)
					 		(eq
					 			(aref (board-board board) 5 (pos-col pos))
					     		'VV
					     	)
					 	)
						(add-pos positions 4 (pos-col pos))
					)
			    )
		    )
		)
    	positions
    )
)

;
;
;@param board 
;@param pos
;@param positions
(defun possible-free-pawn (board pos positions)
	"Lugares a los que un peón puede moverse"
  	(dolist (a-pos (free-pos-pawn board pos))
  		(when 
  			(eq 
  				(board-pos (board-board board) a-pos) 
  				'VV
  			)
  			(setf positions (cons a-pos positions))
  		)
  	)
  	positions
)